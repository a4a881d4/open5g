package test

import open5g.tools.edif._
import scala.io.Source
import java.io.PrintWriter
import java.io.File

object count {
  def main(args:Array[String]) {
    val s = "(123)"
    val (l,e) = handwork.count(s,0,0)
    println(l,e)
  }
}

object lib {
  def main(args:Array[String]) {
    val path = getClass.getResource("/zcpsm.edf").getFile

    val source = Source.fromFile(path)
    val lines = source.mkString
    val r = handwork.getLibs(lines)
    println(r)
  }
}

object scan {
  def main(args:Array[String]) {
    val path = if (args.length > 1) args(1) else getClass.getResource("/zcpsm.edf").getFile

    val source = Source.fromFile(path)
    val lines = source.mkString
    val hw = handwork(lines)
    val pcell = hw.getCell(hw.getLib("hdi_primitives")).map{x => (x.name -> x)}.toMap
    val wcell = hw.getCell(hw.getLib("work")).map{x => (x.name -> x)}.toMap
    val cell = if (args.length > 2) args(2) else "zcpsm"
    // wcell(cell).nets.take(10).foreach(println)
    wcell(cell).instances.foreach{case (x,v) => if(x.contains("core_aRegfileCNU")) println(x,v)}
    println("---")
    val G = hw.cell2G(wcell(cell),(pcell ++ wcell))
    args.foreach(println)
    val p = new PrintWriter(new File(args(0)))
    G.foreach(x => hw.G2String(x,p))
    p.close()
  }

}

object instance {
  def main(args:Array[String]) {
    val path = if (args.length > 1) args(1) else getClass.getResource("/zcpsm.edf").getFile

    val source = Source.fromFile(path)
    val lines = source.mkString
    val hw = handwork(lines)
    val wins = hw.getIns(hw.getLib("work"))
    val cell = if (args.length > 2) args(2) else "zcpsm"
    val p = new PrintWriter(new File(args(0)))
    hw.I2String(wins(cell),p)
    p.close()
  }

}
